//
//  ViewController.m
//  Recipe Scrollview
//
//  Created by Alex Brown on 5/31/15.
//  Copyright (c) 2015 NInjas Inc. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    
    //Scrollview
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    [self.view addSubview:scrollView];
    CGSize scrollViewContentSize = CGSizeMake(1950, 600);
    [scrollView setContentSize:scrollViewContentSize];
    
    //Crepe
    UILabel  * lblCrepe = [[UILabel alloc] initWithFrame:CGRectMake(40, 30, 300, 50)];
    lblCrepe.backgroundColor = [UIColor clearColor];
    lblCrepe.textAlignment = NSTextAlignmentCenter;
    lblCrepe.textAlignment = NSTextAlignmentCenter;
    lblCrepe.textColor=[UIColor blackColor];
    lblCrepe.text = @"Crepe";
    [scrollView addSubview:lblCrepe];
    
    UIImageView *crepe =[[UIImageView alloc] initWithFrame:CGRectMake(20,70,300,120)];
    crepe.image=[UIImage imageNamed:@"crepe"];
    [scrollView addSubview:crepe];
    
    UILabel  * lblCrepeInfo = [[UILabel alloc] initWithFrame:CGRectMake(40, 200, 300, 300)];
    lblCrepeInfo.backgroundColor = [UIColor clearColor];
    lblCrepeInfo.textAlignment = NSTextAlignmentCenter;
    lblCrepeInfo.textColor=[UIColor blackColor];
    lblCrepeInfo.numberOfLines = 20;
    lblCrepeInfo.lineBreakMode = NSLineBreakByWordWrapping;
    lblCrepeInfo.text = @"In a large mixing bowl, whisk together the flour and the eggs. Gradually add in the milk and water, stirring to combine. Add the salt and butter; beat until smooth. Heat a lightly oiled griddle or frying pan over medium high heat. Pour or scoop the batter onto the griddle, using approximately 1/4 cup for each crepe. Tilt the pan with a circular motion so that the batter coats the surface evenly. Cook the crepe for about 2 minutes, until the bottom is light brown. Loosen with a spatula, turn and cook the other side. Serve hot.";
    [scrollView addSubview:lblCrepeInfo];
    
    //French Toast
    UILabel  * lblFrenchToast = [[UILabel alloc] initWithFrame:CGRectMake(40, 30, 1000, 50)];
    lblFrenchToast.backgroundColor = [UIColor clearColor];
    lblFrenchToast.textAlignment = NSTextAlignmentCenter;
    lblFrenchToast.textColor=[UIColor blackColor];
    lblFrenchToast.text = @"French Toast";
    [scrollView addSubview:lblFrenchToast];
    
    UIImageView *FrenchToast =[[UIImageView alloc] initWithFrame:CGRectMake(400,70,300,120)];
    FrenchToast.image=[UIImage imageNamed:@"ft"];
    [scrollView addSubview:FrenchToast];
    
    UILabel  * lblFTInfo = [[UILabel alloc] initWithFrame:CGRectMake(400, 200, 300, 300)];
    lblFTInfo.backgroundColor = [UIColor clearColor];
    lblFTInfo.textAlignment = NSTextAlignmentCenter;
    lblFTInfo.textColor=[UIColor blackColor];
    lblFTInfo.numberOfLines = 15;
    lblFTInfo.lineBreakMode = NSLineBreakByWordWrapping;
    lblFTInfo.text = @"In a small bowl, combine, cinnamon, nutmeg, and sugar and set aside briefly.In a 10-inch or 12-inch skillet, melt butter over medium heat. Whisk together cinnamon mixture, eggs, milk, and vanilla and pour into a shallow container such as a pie plate. Dip bread in egg mixture. Fry slices until golden brown, then flip to cook the other side. Serve with syrup.";
    [scrollView addSubview:lblFTInfo];
   
    //Omelette
    UILabel  * lblOmelette = [[UILabel alloc] initWithFrame:CGRectMake(40, 30, 1700, 50)];
    lblOmelette.backgroundColor = [UIColor clearColor];
    lblOmelette.textAlignment = NSTextAlignmentCenter;
    lblOmelette.textColor=[UIColor blackColor];
    lblOmelette.text = @"Omelette";
    [scrollView addSubview:lblOmelette];
    
    UIImageView *Omelette =[[UIImageView alloc] initWithFrame:CGRectMake(800,70,300,120)];
    Omelette.image=[UIImage imageNamed:@"omelette"];
    [scrollView addSubview:Omelette];
    
    UILabel  * lblOmeletteInfo = [[UILabel alloc] initWithFrame:CGRectMake(800, 200, 300, 300)];
    lblOmeletteInfo.backgroundColor = [UIColor clearColor];
    lblOmeletteInfo.textAlignment = NSTextAlignmentCenter;
    lblOmeletteInfo.textColor=[UIColor blackColor];
    lblOmeletteInfo.numberOfLines = 15;
    lblOmeletteInfo.lineBreakMode = NSLineBreakByWordWrapping;
    lblOmeletteInfo.text = @"Crack the eggs into a mixing bowl with a pinch of salt and pepper, Beat well with a fork. Put a small frying pan on a low heat and let it get hot, add a small knob of butte";
    [scrollView addSubview:lblOmeletteInfo];
    
    //Oatmeal
    UILabel  * lblOatmeal = [[UILabel alloc] initWithFrame:CGRectMake(40, 30, 2400, 50)];
    lblOatmeal.backgroundColor = [UIColor clearColor];
    lblOatmeal.textAlignment = NSTextAlignmentCenter;
    lblOatmeal.textColor=[UIColor blackColor];
    lblOatmeal.text = @"Oatmeal";
    [scrollView addSubview:lblOatmeal];
    
    UIImageView *Oatmeal =[[UIImageView alloc] initWithFrame:CGRectMake(1200,70,300,120)];
    Oatmeal.image=[UIImage imageNamed:@"oatmeal"];
    [scrollView addSubview:Oatmeal];
    
    UILabel  * lblOatmealInfo = [[UILabel alloc] initWithFrame:CGRectMake(1200, 200, 300, 300)];
    lblOatmealInfo.backgroundColor = [UIColor clearColor];
    lblOatmealInfo.textAlignment = NSTextAlignmentCenter;
    lblOatmealInfo.textColor=[UIColor blackColor];
    lblOatmealInfo.numberOfLines = 15;
    lblOatmealInfo.lineBreakMode = NSLineBreakByWordWrapping;
    lblOatmealInfo.text = @"Crack the eggs into a mixing bowl with a pinch of salt and pepper, Beat well with a fork. Put a small frying pan on a low heat and let it get hot, add a small knob of butte";
    [scrollView addSubview:lblOatmealInfo];
    
    //Waffles
    UILabel  * lblWaffles = [[UILabel alloc] initWithFrame:CGRectMake(40, 30, 3400, 50)];
    lblWaffles.backgroundColor = [UIColor clearColor];
    lblWaffles.textAlignment = NSTextAlignmentCenter;
    lblWaffles.textColor=[UIColor blackColor];
    lblWaffles.text = @"Waffles";
    [scrollView addSubview:lblWaffles];
    
    UIImageView *Waffles =[[UIImageView alloc] initWithFrame:CGRectMake(1600,70,300,120)];
    Waffles.image=[UIImage imageNamed:@"waffles"];
    [scrollView addSubview:Waffles];
    
    UILabel  * lblWafflesInfo = [[UILabel alloc] initWithFrame:CGRectMake(1600, 200, 300, 300)];
    lblWafflesInfo.backgroundColor = [UIColor clearColor];
    lblWafflesInfo.textAlignment = NSTextAlignmentCenter;
    lblWafflesInfo.textColor=[UIColor blackColor];
    lblWafflesInfo.numberOfLines = 15;
    lblWafflesInfo.lineBreakMode = NSLineBreakByWordWrapping;
    lblWafflesInfo.text = @"Preheat waffle iron. Beat eggs in large bowl with hand beater until fluffy. Beat in flour, milk, vegetable oil, sugar, baking powder, salt and vanilla, just until smooth. Spray preheated waffle iron with non-stick cooking spray. Pour mix onto hot waffle iron. Cook until golden brown. Serve hot.";
    [scrollView addSubview:lblWafflesInfo];
    
        [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
